#include <fstream>
#include <iostream>

#include "ndnssec/core/abstract-producer.hpp"

#include <ndn-cxx/data.hpp>
#include <ndn-cxx/key-locator.hpp>
#include <ndn-cxx/signature.hpp>

NDN_LOG_INIT(de.fuberlin.ndnssec.MaliciousProducer);

using namespace nonstd;

namespace ndnssec {
namespace examples {

class Producer : public core::AbstractProducer
{
public:
  Producer(std::string producerIdentity)
  {
    identity = producerIdentity;
  };

private:
  void
  onInterest(const ndn::InterestFilter& filter, const ndn::Interest& interest)
  {
    // Create new name, based on Interest's name
    ndn::Name dataName(interest.getName());

    const std::string content = "TEST PAYLOAD";

    // Create Data packet
    ndn::shared_ptr<ndn::Data> data = ndn::make_shared<ndn::Data>();
    data->setName(dataName);
    data->setFreshnessPeriod(ndn::time::seconds(10));
    data->setContent(reinterpret_cast<const uint8_t*>(content.c_str()), content.size());

    ndn::security::SigningInfo info;
    ndn::SignatureInfo signatureInfo;
    ndn::KeyLocator keyLocator;

    // use correct fingerprint
    keyLocator.setName(identity.toUri() + "/KEY/malicious");

    signatureInfo.setKeyLocator(keyLocator);
    signatureInfo.setSignatureType(ndn::tlv::SignatureSha256WithRsa);

    data->setSignature(ndn::Signature(signatureInfo));

    ndn::EncodingBuffer encoder;
    data->wireEncode(encoder, true);

    try {
      // Calculate digest correctly
      ndn::ConstBufferPtr signature =
        keyChain.getTpm().sign(encoder.buf(),
                               encoder.size(),
                               keyChain.getPib().getIdentity("/malicious").getDefaultKey().getName(),
                               ndn::DigestAlgorithm::SHA256);
      ndn::Block signatureValue(ndn::tlv::SignatureValue, signature);
      data->wireEncode(encoder, signatureValue);
      face.put(*data);

      NDN_LOG_TRACE("[X] NDN,A," << interest.getName());
    }
    catch (const std::exception& e) {
      NDN_LOG_TRACE("[X] NDN,E," << interest.getName() << "," << e.what());
    }
  }

  void
  onRegisterFailed(const ndn::Name& prefix, const std::string& reason)
  {
    NDN_LOG_ERROR("[X] NDN,E,Failed to register prefix \"" << prefix << "\" in local hub's daemon ("
                                                        << reason << ")");
    face.shutdown();
  }
};

} // end namespace examples
} // end namespace ndnssec

int
main(int argc, char** argv)
{
  std::string identity = std::string(argv[1]);
  ndnssec::examples::Producer producer(identity);
  try {
    producer.run(identity);
  }
  catch (const std::exception& exception) {
    // MARK - exception error
    NDN_LOG_ERROR("[X] NDN,E," << exception.what());
  }
  return 0;
}