#ifndef NDNSSEC_CORE_ABSTRACT_PRODUCER_HPP
#define NDNSSEC_CORE_ABSTRACT_PRODUCER_HPP

#include <ndn-cxx/face.hpp>
#include <ndn-cxx/interest-filter.hpp>
#include <ndn-cxx/interest.hpp>
#include <ndn-cxx/name.hpp>
#include <ndn-cxx/security/signing-info.hpp>
#include <ndn-cxx/security/v2/key-chain.hpp>
#include <ndn-cxx/util/logger.hpp>

#include <memory>

using namespace nonstd;

namespace ndnssec {
namespace core {

/**
 * @brief Base class for producers wishing to use NDNSSEC.
 *
 * Some basic assumptions are made:
 * - Data packets are signed
 * - Private key of the producer must be available in local NDN KeyChain
 * - Support for keys is limited to RSA
 * - Signatures are RSA signatures over SHA256 hashes
 *
 * @author Pouyan Fotouhi Tehrani
 */
class AbstractProducer : ndn::noncopyable
{
public:
  /**
   * @brief      Starts listening for incoming interests.
   *
   * @param[in] filter interest filter
   */
  void
  run(const std::string& filter)
  {
    face.setInterestFilter(filter,
                           bind(&AbstractProducer::onInterest, this, _1, _2),
                           ndn::RegisterPrefixSuccessCallback(),
                           bind(&AbstractProducer::onRegisterFailed, this, _1, _2));
    face.processEvents();
  }

protected:
  /**
   * @brief     Callback for matching interests
   *
   * @param[in] filter interest filter
   * @param[in] interest
   */
  virtual void
  onInterest(const ndn::InterestFilter& filter, const ndn::Interest& interest) = 0;

  /**
   * @brief     Callback when registring an interest fails
   *
   * @param[in] prefix failed to register prefix
   * @param[in] reason failure reason
   */
  virtual void
  onRegisterFailed(const ndn::Name& prefix, const std::string& reason) = 0;

  /**
   * @brief                   sings given data packet.
   *
   * @param[in,out] data      packet to be signed
   */
  void
  signData(ndn::Data& data)
  {
    ndn::security::SigningInfo info;
    info.setSigningIdentity(identity);
    keyChain.sign(data, info);
  }

protected:
  ndn::Name identity;
  ndn::Face face;
  ndn::security::v2::KeyChain keyChain;
};

} // end namespace core
} // end namespace ndnssec

#endif // NDNSSEC_CORE_ABSTRACT_PRODUCER_HPP
