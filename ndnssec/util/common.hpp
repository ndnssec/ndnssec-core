#ifndef NDNSSEC_UTIL_COMMON_HPP
#define NDNSSEC_UTIL_COMMON_HPP

#include <memory>

#include <ndn-cxx/name.hpp>
#include <ndn-cxx/security/transform/buffer-source.hpp>
#include <ndn-cxx/security/transform/hex-encode.hpp>
#include <ndn-cxx/security/transform/stream-sink.hpp>

#include <ldns/ldns.h>

namespace ndnssec {
namespace util {

class Common
{
public:
  /**
   * @brief      Generates a digest using same algorithm used for DS records.
   *
   * It is expected that the given key is generated from a DNSKEY record so that
   * attributes such as owner, flags, etc. are correctly set.
   *
   * @param[in]  k   The public key
   *
   * @return     Digest of given key.
   */
  std::shared_ptr<std::string> static generateFingerprint(ldns_rr* publicKey)
  {
    // Create a DS record for given DNSKEY
    ldns_rr* ds = ldns_key_rr2ds(publicKey, LDNS_SHA1);
    // Pop out the digest only and leave the rest out
    ldns_rdf* digest = ldns_rr_pop_rdf(ds);

    // Hex encode and return the results
    std::stringstream digestStream;
    using namespace ndn::security::transform;
    bufferSource(ldns_rdf_data(digest), ldns_rdf_size(digest)) >> hexEncode() >>
      streamSink(digestStream);
    std::string digestStr = digestStream.str();

    return ndn::make_shared<std::string>(digestStr);
  }

  /**
   * @brief      Generates an hostname from ndn prefix name
   *
   * @param[in]  hostname   name to be transformed
   *
   * @return     String of hostname
   */
  std::string static generateHostname(const ndn::Name hostname)
  {
    std::string hostnameString = "";

    for (std::reverse_iterator<const ndn::name::Component*> iterator = hostname.rbegin();
         iterator != hostname.rend();
         ++iterator) {
      hostnameString.append((*iterator).toUri()).append(".");
    }

    return hostnameString;
  }
};

} // end namespace util
} // end namespace ndnssec

#endif // NDNSSEC_UTIL_COMMON_HPP
