#include <fstream>

#include "common.hpp"
#include "dns-fetcher.hpp"

#include <ndn-cxx/util/logger.hpp>

NDN_LOG_INIT(de.fuberlin.ndnssec.DnsHelper);

using namespace ndnssec::util;
using namespace nonstd;

DnsFetcher::DnsFetcher(const std::string keyFile, char* resolvFile = nullptr)
  : m_keyFile(keyFile)
  , m_resolvFile(resolvFile)
  , m_keyList(ldns_rr_list_new())
{
}

errorCode
DnsFetcher::readKeyFile(const char* filename)
{
  ldns_status status;
  ldns_rr* rr;
  int keyCount = 0;

  std::ifstream file(filename);
  if (file.is_open()) {
    std::string line;
    while (std::getline(file, line)) {
      if (line.length() > 0 && *line.cbegin() != ';') {
        status = ldns_rr_new_frm_str(&rr, line.c_str(), 0, nullptr, nullptr);
        // Go ahead and process valid lines only
        if (LDNS_STATUS_OK == status) {
          ldns_rr_type type = ldns_rr_get_type(rr);
          if (LDNS_RR_TYPE_DNSKEY == type || LDNS_RR_TYPE_DS == type) {
            ldns_rr_list_push_rr(m_keyList, rr);
            keyCount++;
          }
          else {
            ldns_rr_free(rr);
          }
        }
      }
    }
    file.close();
  }
  else {
    return errorCode::CANNOT_OPEN_TRUST_ANCHOR;
  }

  // If no keyCount is still zero, either:
  // i) no DNSKEY or DS records were found OR
  // ii) RR string was malformed (i.e, LDNS_STATUS_SYNTAX_*_ERR)
  return (keyCount > 0) ? errorCode::NO_ERROR : errorCode::ANCHOR_PARSE_FAIL;
}

errorCode
DnsFetcher::initializeLDNS()
{
  ldns_status status;

  status = ldns_resolver_new_frm_file(&m_resolver, m_resolvFile);
  if (LDNS_STATUS_OK != status) {
    return errorCode::SET_STUB_RESOLVER_FAIL;
  }

  // Parse trusted DNSKEY/DS RRs
  errorCode error = readKeyFile(m_keyFile.c_str());
  if (errorCode::NO_ERROR != error) {
    return error;
  }

  ldns_resolver_set_dnssec(m_resolver, true);
  ldns_resolver_set_dnssec_anchors(m_resolver, ldns_rr_list_clone(m_keyList));

  return errorCode::NO_ERROR;
}

errorCode
DnsFetcher::verifyDenial(ldns_pkt* packet, ldns_rdf* name)
{
  ldns_rr_list* nsecs;
  ldns_status status = LDNS_STATUS_OK;

  // Make sure RRSIGs are given
  ldns_rr_list* sigs =
    ldns_pkt_rr_list_by_type(packet, LDNS_RR_TYPE_RRSIG, LDNS_SECTION_ANY_NOQUESTION);
  if (nullptr == sigs) {
    return errorCode::DNS_NO_SIGNATURE_FOUND;
  }

  // Reconstruct the query
  ldns_rr* query = ldns_rr_new();
  if (nullptr == query) {
    return errorCode::MEMORY_ERROR;
  }
  ldns_rr_set_question(query, 1);
  ldns_rr_set_ttl(query, 0);
  ldns_rr_set_owner(query, ldns_rdf_clone(name));
  ldns_rr_set_type(query, LDNS_RR_TYPE_DNSKEY);

  nsecs = ldns_pkt_rr_list_by_type(packet, LDNS_RR_TYPE_NSEC, LDNS_SECTION_ANY_NOQUESTION);
  if (nullptr != nsecs) {
    // First check NSEC
    status = ldns_dnssec_verify_denial(query, nsecs, sigs);
  }
  else if ((nsecs =
              ldns_pkt_rr_list_by_type(packet, LDNS_RR_TYPE_NSEC3, LDNS_SECTION_ANY_NOQUESTION))) {
    // Then check NSEC3
    status = ldns_dnssec_verify_denial_nsec3_match(query, nsecs, sigs, ldns_pkt_get_rcode(packet),
                                                   LDNS_RR_TYPE_DNSKEY,
                                                   ldns_pkt_ancount(packet) == 0, nullptr);
  }

  ldns_rr_list_deep_free(sigs);
  ldns_rr_free(query);
  ldns_rr_list_deep_free(nsecs);

  if (LDNS_STATUS_OK == status) {
    return errorCode::NO_ERROR;
  }
  else {
    // @TODO: handle specific LDNS errors
    // LDNS_STATUS_DNSSEC_NSEC_RR_NOT_COVERED
    // LDNS_STATUS_DNSSEC_NSEC_WILDCARD_NOT_COVERED
    // LDNS_STATUS_NSEC3_ERR
    NDN_LOG_TRACE(ldns_get_errorstr_by_id(status));
    return errorCode::OTHER;
  }
}

variant<errorCode, ndn::ConstBufferPtr>
DnsFetcher::getPublicKey(const std::string& hostname, const std::string& fingerprint)
{
  variant<errorCode, ndn::ConstBufferPtr> errorReturnStatus;
  errorReturnStatus = getRawPublicKey(hostname, fingerprint);
  if (holds_alternative<errorCode>(errorReturnStatus)) {
    return get<errorCode>(errorReturnStatus);
  }

  ndn::ConstBufferPtr rawKey = get<ndn::ConstBufferPtr>(errorReturnStatus);

  if (nullptr == rawKey) {
    return errorCode::DNS_NO_KEY_MATCH;
  }

  // To desrialize public keys (in DNSKEY) it is preferred to use 'ldns'
  RSA* rsa = ldns_key_buf2rsa_raw(rawKey->data(), rawKey->size());

  EVP_PKEY* publicKey = EVP_PKEY_new();
  EVP_PKEY_set1_RSA(publicKey, rsa);

  uint8_t* pkcs8 = nullptr;
  int length = i2d_PUBKEY(publicKey, &pkcs8);

  // This also releases the underlying RSA key
  EVP_PKEY_free(publicKey);

  return (ndn::ConstBufferPtr)ndn::make_shared<ndn::Buffer>(pkcs8, length);
}

variant<errorCode, ndn::ConstBufferPtr>
DnsFetcher::getRawPublicKey(const std::string& hostname, const std::string& fingerprint)
{
  ndn::ConstBufferPtr match = nullptr;
  errorCode result = errorCode::NO_ERROR;
  ldns_status status;
  ldns_rdf* domain = nullptr;
  ldns_pkt* packet = nullptr;
  ldns_rr_list* keys = nullptr;
  ldns_rr_list* keyVerified = nullptr;

  errorCode error = initializeLDNS();
  if (errorCode::NO_ERROR != error) {
    return error;
  }

  domain = ldns_dname_new_frm_str(hostname.c_str());
  if (nullptr == domain) {
    return errorCode::OTHER;
  }

  status = ldns_resolver_query_status(&packet, m_resolver, domain, LDNS_RR_TYPE_DNSKEY,
                                      LDNS_RR_CLASS_IN, LDNS_RD);
  if (LDNS_STATUS_OK != status) {
    result = errorCode::DNS_FETCH_FAIL;
  }
  else if (nullptr == packet) {
    result = errorCode::DNS_NO_RESPONSE;
  }
  else {
    // Build a trust tree up to root to verify DNSSEC sigs
    ldns_dnssec_data_chain* chain =
      ldns_dnssec_build_data_chain(m_resolver, LDNS_RD,
                                   ldns_pkt_rr_list_by_type(packet, LDNS_RR_TYPE_DNSKEY,
                                                            LDNS_SECTION_ANY_NOQUESTION),
                                   packet, nullptr);

    ldns_dnssec_trust_tree* tree = ldns_dnssec_derive_trust_tree(chain, nullptr);
    status = ldns_dnssec_trust_tree_contains_keys(tree, m_keyList);

    ldns_pkt_rcode rCode = ldns_pkt_get_rcode(packet);

    // Trust tree verification returns EXISTENCE_DENINED *ONLY* for
    // NSEC and STATUS_OK for NSEC3. We vill verify both if response
    // rCode is set to NXDOMAIN
    if (LDNS_STATUS_DNSSEC_EXISTENCE_DENIED == status || LDNS_RCODE_NXDOMAIN == rCode) {
      // is verification failure due to explicit denial?
      errorCode error = verifyDenial(packet, domain);
      if (errorCode::NO_ERROR == error) {
        result = errorCode::DNS_EXISTENCE_DENIED;
      }
      else {
        result = errorCode::DNS_VERIFY_FAILED;
      }
    }
    else if (LDNS_STATUS_OK == status) {
      // Verification succeeded

      // Extract all DNSKEYS from answer section
      keys = ldns_pkt_rr_list_by_type(packet, LDNS_RR_TYPE_DNSKEY, LDNS_SECTION_ANSWER);

      while (ldns_rr_list_rr_count(keys) > 0) {
        ldns_rr* key = ldns_rr_list_pop_rr(keys);
        std::shared_ptr<std::string> digest = util::Common::generateFingerprint(key);

        if (fingerprint.compare(*digest) == 0) {
          ldns_rdf* rawKey = ldns_rr_dnskey_key(key);

          match = (ndn::ConstBufferPtr)ndn::make_shared<ndn::Buffer>(rawKey->_data, rawKey->_size);
        }
      }
    }
    else {
      // @TODO specific LDNS errors are not handled
      NDN_LOG_TRACE(ldns_get_errorstr_by_id(status));
      result = errorCode::DNS_VERIFY_FAILED;
    }
  }

  ldns_rdf_free(domain);
  ldns_pkt_free(packet);
  ldns_rr_list_deep_free(keys);
  ldns_rr_list_deep_free(keyVerified);

  if (errorCode::NO_ERROR == result) {
    return match;
  }
  else {
    return result;
  }
}
