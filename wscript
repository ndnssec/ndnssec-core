def options(opt):
    opt.load('compiler_c compiler_cxx')
    opt.load('default-compiler-flags openssl', tooldir=['.waf-tools'])


def configure(conf):
    conf.load('compiler_c compiler_cxx default-compiler-flags openssl')
    conf.check_openssl()

    conf.check_cfg(package='ldns',
                   args=['--cflags', '--libs'],
                   uselib_store='LDNS',
                   mandatory=True)

    conf.check_cfg(package='libndn-cxx',
                   args=['--cflags', '--libs'], uselib_store='NDN_CXX',
                   mandatory=True)


def build(bld):
    libndnssec = dict(
        target='ndnssec',
        source=bld.path.ant_glob(
            'ndnssec/**/*.cpp', excl=['ndnssec/examples/**/*.cpp']),
        use='NDN_CXX OPENSSL LDNS',
        includes='.',
        export_includes='.',
        install_path='${LIBDIR}')

    bld.stlib(name='ndnssec',
              vnum='0.0.1',
              cnum='0.0.1',
              **libndnssec)

    bld.recurse('examples')
