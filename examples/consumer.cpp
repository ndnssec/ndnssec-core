#include <iostream>

#include "ndn-cxx/security/verification-helpers.hpp"
#include <ndn-cxx/encoding/buffer.hpp>
#include <ndn-cxx/lp/nack.hpp>
#include <ndn-cxx/name.hpp>
#include <ndn-cxx/security/transform/public-key.hpp>

#include "ndnssec/core/abstract-consumer.hpp"

using namespace nonstd;

namespace ndnssec {
namespace examples {

class Consumer : public core::AbstractConsumer
{
public:
  Consumer(std::string keyFile, char* resolvFile)
    : core::AbstractConsumer(keyFile, resolvFile){};

  void
  run(const std::string interestName)
  {
    NDN_LOG_INFO("[X] NDN,Q," << interestName);
    AbstractConsumer::run(interestName);
  }

private:
  void
  onData(const ndn::Interest& interest, const ndn::Data& data)
  {
    ndnssec::core::ErrorHandler handler;

    errorCode code;
    code = verifyData(interest, data);
    if (errorCode::NO_ERROR != code) {
      NDN_LOG_ERROR("[X] NDN,E," << interest.getName() << "," << handler.getErrorMessage(code));
      return;
    }

    NDN_LOG_TRACE("[X] DNS,Q," << data.getName());
    variant<errorCode, bool> errorReturnStatus;
    errorReturnStatus = verifyDataSignature(data);
    if (holds_alternative<errorCode>(errorReturnStatus)) {
      std::string err;
      switch (get<errorCode>(errorReturnStatus)) {
        case DNS_NO_KEY_MATCH:
          err = "No matching key found in NDN!";
          break;
        case DNS_NO_RESPONSE:
          err = "No response received from DNS!";
          break;
        case DNS_EXISTENCE_DENIED:
          err = "Existense of name denied!";
          break;
        case DNS_VERIFY_FAILED:
          err = "DNSSEC chain cannot be validated!";
          break;
        case SET_STUB_RESOLVER_FAIL:
          err = "Could not initialize local stub resolver!";
          break;
        default:
          err = "DNS Error!";
      }
      NDN_LOG_TRACE("[X] NDN,V," << data.getName() << "," << err);
    }
    else {
      NDN_LOG_TRACE("[X] NDN,V," << data.getName() << ",Received packet is valid!");
    }
  }

  void
  onNack(const ndn::Interest& interest, const ndn::lp::Nack& nack)
  {
    NDN_LOG_TRACE("[X] NDN,E," << interest.getName() << ",Nack for interest '" << interest.getName()
                               << "'");
  }

  void
  onTimeout(const ndn::Interest& interest)
  {
    NDN_LOG_TRACE("[X] NDN,E," << interest.getName() << ",Timeout for interest '"
                               << interest.getName() << "'!");
  }
};

} // end namespace examples
} // end namespace ndnssec

int
main(int argc, char** argv)
{
  ndnssec::examples::Consumer consumer(std::string(argv[2]), argv[3]);

  try {
    consumer.run(argv[1]);
  }
  catch (const std::exception& exception) {
    // MARK - exception error
    NDN_LOG_ERROR("[X] NDN,E," << exception.what());
  }

  return 0;
}
