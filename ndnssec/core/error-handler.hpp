#ifndef NDNSSEC_CORE_ERROR_HANDLER_HPP
#define NDNSSEC_CORE_ERROR_HANDLER_HPP

#include <map>
#include <stdint.h>

typedef enum errorCode_t : uint8_t {
  NO_ERROR = 0,
  CANNOT_OPEN_FILE = 1,
  PRIVATE_KEY_PARSE_FAIL = 2,
  PUBLIC_KEY_PARSE_FAIL = 3,
  PUBLIC_KEY_FETCH_FAIL = 4,
  WRONG_DATA_NAME = 5,
  PRIVATE_KEY_CONVERT_FAIL = 6,
  ANCHOR_PARSE_FAIL = 7,
  CANNOT_OPEN_TRUST_ANCHOR = 8,
  DNS_FETCH_FAIL = 9,
  DNS_NO_RESPONSE = 10,
  SET_STUB_RESOLVER_FAIL = 11,
  DNS_NO_SIGNATURE_FOUND = 12,
  DNS_EXISTENCE_DENIED = 13,
  DNS_VERIFY_FAILED = 14,
  MEMORY_ERROR = 15,
  DNS_NO_KEY_MATCH = 16,
  LDNS_ERROR_CASE = 17,
  OTHER = 255
} errorCode;


namespace ndnssec {
namespace core {

class ErrorHandler
{
public:
  ErrorHandler()
  {
    m_errorCase[NO_ERROR] = "All OK";
    m_errorCase[CANNOT_OPEN_FILE] = "Could not open file with fopen()";
    m_errorCase[PRIVATE_KEY_PARSE_FAIL] = "Could not transform private key using ldns";
    m_errorCase[PUBLIC_KEY_PARSE_FAIL] = "Could not transform public key using ldns";
    m_errorCase[PUBLIC_KEY_FETCH_FAIL] = "Could not get public key from fetcher";
    m_errorCase[WRONG_DATA_NAME] = "Data name is not same as keylocator name";
    m_errorCase[PRIVATE_KEY_CONVERT_FAIL] = "Could not convert key into PKCS #8 format";
    m_errorCase[ANCHOR_PARSE_FAIL] = "Could not parse trust anchor file";
    m_errorCase[CANNOT_OPEN_TRUST_ANCHOR] = "Could not open trust anchor file";
    m_errorCase[DNS_FETCH_FAIL] = "Could not fetch dns response";
    m_errorCase[DNS_NO_RESPONSE] = "Fetched dns response is empty";
    m_errorCase[SET_STUB_RESOLVER_FAIL] = "Could not confiugre resolver";
    m_errorCase[DNS_NO_SIGNATURE_FOUND] = "Could not find RRSIG in dns response";
    m_errorCase[DNS_EXISTENCE_DENIED] = "Existence denied while verification attempt";
    m_errorCase[DNS_VERIFY_FAILED] = "Verification failure due to not existent domain";
    m_errorCase[MEMORY_ERROR] = "Memory error while creating rr structure";
    m_errorCase[DNS_NO_KEY_MATCH] = "No key match in dns response found";
    m_errorCase[LDNS_ERROR_CASE] = "ldns specific error occured";
    m_errorCase[OTHER] = "Not reviewed or default error case occured";
  }

public:
  std::string
  getErrorMessage(uint8_t id)
  {
    return m_errorCase[id];
  }

private:
  std::map<uint8_t, std::string> m_errorCase;
};

} // end namespace core
} // end namespace ndnssec

#endif // NDNSSEC_CORE_ERROR_HANDLER_HPP