#include "createcert.hpp"

#include "ndnssec/core/error-handler.hpp"
#include "ndnssec/util/common.hpp"

#include <iostream>

#include <ndn-cxx/util/io.hpp>
#include <ndn-cxx/util/logger.hpp>

NDN_LOG_INIT(de.fuberlin.ndnssec.CreationTool);

using namespace ndnssec;
using namespace nonstd;

CreationTool::CreationTool(const std::string& identityName)
  : m_identityName(identityName)
  , m_privateKey(nullptr)
  , m_publicKey(nullptr)
{
  #if OPENSSL_VERSION_NUMBER < 0x1010000fL
    static bool isInitialized = false;
    if (!isInitialized) {
      OpenSSL_add_all_algorithms();
      isInitialized = true;
    }
  #endif // OPENSSL_VERSION_NUMBER < 0x1010000fL
}

CreationTool::~CreationTool(void)
{
  ldns_key_free(m_privateKey);
  ldns_rr_free(m_publicKey);
  // This also free's the underlying RSA key generated
  // in 'parseRSAKey' method.
  EVP_PKEY_free(m_key);
}

errorCode
CreationTool::createSafebag(const std::string& keyPath)
{
  errorCode errorReturnStatus = parsePrivateKey(keyPath + ".private");

  if (errorCode::NO_ERROR != errorReturnStatus) {
    return errorReturnStatus;
  }

  errorReturnStatus = parsePublicKey(keyPath + ".key");
  if (errorCode::NO_ERROR != errorReturnStatus) {
    return errorReturnStatus;
  }

  RSA* rsa = ldns_key_rsa_key(m_privateKey);
  m_key = EVP_PKEY_new();
  EVP_PKEY_assign_RSA(m_key, rsa);

  NDN_LOG_TRACE("Creating private key...");
  variant<errorCode, ndn::ConstBufferPtr> privateKeyStatus;
  privateKeyStatus = generatePrivateKey("test");
  if (holds_alternative<errorCode>(privateKeyStatus)) {
    return get<errorCode>(privateKeyStatus);
  }
  ndn::ConstBufferPtr privateKey = get<ndn::ConstBufferPtr>(privateKeyStatus);

  NDN_LOG_TRACE("Creating public key...");
  ndn::ConstBufferPtr publicKey = serializePublicKey();

  NDN_LOG_TRACE("Creating certificate...");
  ndn::security::v2::Certificate certificate;

  // set name
  // This corresponds to the following format
  // /<NameSpace>/KEY/[KeyId]/[IssuerId]/[Version]
  ndn::Name certificateName = ndn::Name();
  certificateName.append(m_identityName)
    .append(ndn::security::v2::Certificate::KEY_COMPONENT)
    .append(*util::Common::generateFingerprint(m_publicKey))
    .append("CHANGE-THIS")
    .appendVersion();
  certificate.setName(certificateName);

  // set metainfo
  certificate.setContentType(ndn::tlv::ContentType_Key);
  certificate.setFreshnessPeriod(ndn::time::hours(1));

  // set content
  certificate.setContent(publicKey->data(), publicKey->size());

  // set signature-info
  // To avoid requiring an extra or the default identity to sign
  // the certificate, the SigningInfo is simply set to 'Sha256Signing'
  // so that a simple SHA256 digest will be used as signature.
  ndn::SignatureInfo signatureInfo;
  signatureInfo.setValidityPeriod(
    ndn::security::ValidityPeriod(ndn::time::system_clock::TimePoint(),
                                  ndn::time::system_clock::now() + ndn::time::days(20 * 365)));
  ndn::security::SigningInfo info;
  info.setSha256Signing();
  info.setSignatureInfo(signatureInfo);
  m_keyChain.sign(certificate, info);

  ndn::security::SafeBag safeBag(certificate, *privateKey);
  ndn::io::save(safeBag, std::cout);

  return errorCode::NO_ERROR;
}

errorCode
CreationTool::parsePrivateKey(const std::string& keyPath)
{
  NDN_LOG_TRACE("Trying to parse privateKey at " << keyPath);
  FILE* privateFile = std::fopen(keyPath.c_str(), "r");
  // TODO std::holds_alternative<std::string>(response)
  if (nullptr == privateFile) {
    NDN_LOG_ERROR("Could not open private key '" << keyPath << "'!");
    return errorCode::CANNOT_OPEN_FILE;
  }

  ldns_status status = ldns_key_new_frm_fp(&m_privateKey, privateFile);
  fclose(privateFile);

  if (LDNS_STATUS_OK != status) {
    NDN_LOG_ERROR("Could not transform private key using ldns! ("
                  << std::string(ldns_get_errorstr_by_id(status)) << ")");
    return errorCode::PRIVATE_KEY_PARSE_FAIL;
  }
  return errorCode::NO_ERROR;
}

errorCode
CreationTool::parsePublicKey(const std::string& keyPath)
{
  NDN_LOG_TRACE("Trying to parse publicKey at " << keyPath);
  FILE* publicFile = std::fopen(keyPath.c_str(), "r");
  if (nullptr == publicFile) {
    NDN_LOG_ERROR("Could not open public key '" + keyPath + "'!");
    return errorCode::CANNOT_OPEN_FILE;
  }

  ldns_status status = ldns_rr_new_frm_fp(&m_publicKey, publicFile, 0, nullptr, nullptr);
  fclose(publicFile);

  if (LDNS_STATUS_OK != status) {
    NDN_LOG_ERROR("Could not transform public key using ldns! (" +
                  std::string(ldns_get_errorstr_by_id(status)) + ")");
    return errorCode::PUBLIC_KEY_PARSE_FAIL;
  }
  return errorCode::NO_ERROR;
}

ndn::ConstBufferPtr
CreationTool::serializePublicKey()
{
  // This is the same as PublicKey::toPkcs8
  uint8_t* pkcs8 = nullptr;
  int len = i2d_PUBKEY(m_key, &pkcs8);

  ndn::BufferPtr buffer = ndn::make_shared<ndn::Buffer>(len);
  std::memcpy(buffer->data(), pkcs8, len);
  OPENSSL_free(pkcs8);

  return (ndn::ConstBufferPtr)buffer;
}

variant<errorCode, ndn::ConstBufferPtr>
CreationTool::generatePrivateKey(const std::string& password)
{
  // The following is a modified version of 'PrivateKey::toPkcs8' method
  // As of version 0.6.5, PKCS#8 are encrypted using AES-256 for export
  // @TODO: CIPHER TYPE MIGHT CHANGE IN FUTURE VERSIONS OF 'ndn-cxx'!
  // @see:
  // https://web.archive.org/web/20190325105322/http://named-data.net/doc/ndn-cxx/0.6.5/release-notes/release-notes-0.6.5.html#improvements-and-bug-fixes
  BIO* memoryBio = BIO_new(BIO_s_mem());
  if (!i2d_PKCS8PrivateKey_bio(memoryBio,
                               m_key,
                               EVP_aes_256_cbc(),
                               nullptr,
                               0,
                               nullptr,
                               const_cast<char*>(password.c_str()))) {
    NDN_LOG_ERROR("Cannot convert key into PKCS #8 format");
    return errorCode::PRIVATE_KEY_CONVERT_FAIL;
  }

  ndn::BufferPtr buffer = ndn::make_shared<ndn::Buffer>(BIO_pending(memoryBio));
  BIO_read(memoryBio, buffer->data(), buffer->size());
  BIO_vfree(memoryBio);

  return (ndn::ConstBufferPtr)buffer;
}

int
main(int argc, char** argv)
{
  if (argc != 3) {
    fprintf(stderr, "Usage: %s <identity name (DNS zone)> <DNSSEC key file prefix>\n", argv[0]);
    return 1;
  }

  try {
    CreationTool creationTool(argv[1]);
    errorCode errorCode;
    errorCode = creationTool.createSafebag(argv[2]);
    if (errorCode::NO_ERROR != errorCode) {
      NDN_LOG_ERROR("Error occured with Code: " << errorCode);
    }
  }
  catch (const std::exception& exception) {
    // MARK - exception error
    NDN_LOG_ERROR("ERROR: " << exception.what());
    return 1;
  }
  return 0;
}