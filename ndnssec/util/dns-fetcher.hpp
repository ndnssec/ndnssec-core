#ifndef NDNSSEC_UTIL_DNS_FETCHER
#define NDNSSEC_UTIL_DNS_FETCHER

#include <cstdio>
#include <iostream>

#include <ldns/ldns.h>

#include <openssl/evp.h>
#include <openssl/rsa.h>
#include <openssl/x509.h>

#include <ndn-cxx/detail/common.hpp>
#include <ndn-cxx/encoding/buffer.hpp>

#include "ndnssec/core/error-handler.hpp"

using namespace nonstd;

namespace ndnssec {
namespace util {
class DnsFetcher
{
public:
  /**
   * @brief Constructs.
   *
   * @param keyFile file with trusted DNSKEY for DNSSEC response verifications (e.g., /etc/unbound/getdns-root.key)
   */
  DnsFetcher(const std::string keyFile, char* resolvFile);

  /**
   * @brief Fetches a key with the fingerprint from the DNS zone of hostname, returns
   *        a properly encoded public key according to the X.509 SubjectPublicKeyInfo
   *        format. If no matching key has been found, a nullptr is returned.
   *
   * @param hostname
   * @param fingerprint
   * @return BufferPtr
   */
  variant<errorCode, ndn::ConstBufferPtr>
  getPublicKey(const std::string& hostname, const std::string& fingerprint);

private:
  /**
   * @brief Initializes the LDNS resolver.
   *
   * @return errorCode NO_ERROR if everything goes well, otherwise SET_STUB_RESOLVER_FAIL.
   */
  errorCode
  initializeLDNS();

  /**
   * @brief Fetches a key with the fingerprint from the DNS zone of hostname, performs a
   *        DNSSEC validation and returns the raw key if a key has been found and the
   *        validation completed successfully.
   *
   * @param hostname
   * @param fingerprint
   * @return BufferPtr
   */
  variant<errorCode, ndn::ConstBufferPtr>
  getRawPublicKey(const std::string& hostname, const std::string& fingerprint);

  /**
   * @brief Reads a key file comprising DNSKEY/DS resource records and adds
   *        them as trusted keys.
   * @param filename path to key file
   * @return NO_ERROR if parsing succeeds, otherwiese CANNOT_OPEN_TRUST_ANCHOR or ANCHOR_PARSE_FAIL
   */
  errorCode
  readKeyFile(const char* filename);

  /**
   * @brief Checks if the existense for a packet is explicitly denied (NSEC/NSEC3).
   *
   * @param pkt DNS response packet
   * @param name domain name
   * @return NO_ERROR if denied, otherwise OTHER errorCode
   */
  errorCode
  verifyDenial(ldns_pkt* packet, ldns_rdf* name);

private:
  // Path to trusted key file containing DNSKEY/DS RRs
  std::string m_keyFile;
  char* m_resolvFile;
  // LDNS resolver
  ldns_resolver* m_resolver;
  // list of keys used in dnssec operations
  ldns_rr_list* m_keyList;
};

} // end namespace util
} // end namespace ndnssec

#endif // NDNSSEC_UTIL_DNS_FETCHER