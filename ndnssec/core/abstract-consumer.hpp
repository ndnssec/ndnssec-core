#ifndef NDNSSEC_CORE_ABSTRACT_CONSUMER_HPP
#define NDNSSEC_CORE_ABSTRACT_CONSUMER_HPP

#include <ndn-cxx/detail/common.hpp>
#include <ndn-cxx/face.hpp>
#include <ndn-cxx/util/logger.hpp>

#include "ndnssec/core/error-handler.hpp"
#include "ndnssec/util/common.hpp"
#include "ndnssec/util/dns-fetcher.hpp"

NDN_LOG_INIT(de.fuberlin.ndnssec.Consumer);

using namespace nonstd;

namespace ndnssec {
namespace core {

/**
 * @brief Base class for consumers wishing to use NDNSSEC.
 */
class AbstractConsumer : ndn::noncopyable
{
public:
  AbstractConsumer(std::string keyFile, char* resolvFile)
    : m_keyFile(keyFile)
    , m_resolvFile(resolvFile){};

  void
  run(const std::string interestName)
  {
    ndn::Interest interest(interestName);
    interest.setCanBePrefix(true);
    interest.setMustBeFresh(true);

    m_face.expressInterest(interest, bind(&AbstractConsumer::onData, this, _1, _2),
                           bind(&AbstractConsumer::onNack, this, _1, _2),
                           bind(&AbstractConsumer::onTimeout, this, _1));

    // processEvents will block until the requested data received or timeout
    // occurs
    m_face.processEvents();
  }

protected:
  /**
   * @brief     Callback for handling data packet
   *
   * @param[in] interest
   * @param[in] data
   */
  virtual void
  onData(const ndn::Interest& interest, const ndn::Data& data) = 0;

  /**
   * @brief     Callback for nack data packet
   *
   * @param[in] interest
   * @param[in] nack
   */
  virtual void
  onNack(const ndn::Interest& interest, const ndn::lp::Nack& nack) = 0;

  /**
   * @brief     Callback for timeout requests
   *
   * @param[in] interest
   */
  virtual void
  onTimeout(const ndn::Interest& interest) = 0;

  /**
   * @brief             verifies data packet
   *
   * @param[in] data    packet to be signed
   * @return            success of data verification
   */
  errorCode
  verifyData(const ndn::Interest& interest, const ndn::Data& data)
  {
    ndn::Name keyLocatorName = data.getSignature().getKeyLocator().getName();

    if (data.getName().equals(keyLocatorName.getPrefix(KEYLOCATOR_PREFIX_OFFSET)) != 1) {
      return errorCode::WRONG_DATA_NAME;
    }

    return errorCode::NO_ERROR;
  }

  variant<errorCode, bool>
  verifyDataSignature(const ndn::Data& data)
  {
    std::string fingerprint;
    fingerprint =
      data.getSignature().getKeyLocator().getName().getSubName(FINGERPRINT_OFFSET).toUri().substr(1);

    ndn::Name keyLocatorPrefix =
      data.getSignature().getKeyLocator().getName().getPrefix(KEYLOCATOR_PREFIX_OFFSET);
    std::string hostnameString;
    hostnameString = util::Common::generateHostname(keyLocatorPrefix);

    variant<errorCode, ndn::ConstBufferPtr> errorReturnStatus;
    util::DnsFetcher fetcher(m_keyFile, m_resolvFile);
    errorReturnStatus = fetcher.getPublicKey(hostnameString, fingerprint);

    if (holds_alternative<errorCode>(errorReturnStatus)) {
      return get<errorCode>(errorReturnStatus);
    }

    ndn::ConstBufferPtr publicKeyBuffer = get<ndn::ConstBufferPtr>(errorReturnStatus);
    if (nullptr == publicKeyBuffer) {
      return errorCode::PUBLIC_KEY_FETCH_FAIL;
    }

    ndn::security::transform::PublicKey publicKey;
    publicKey.loadPkcs8(publicKeyBuffer->data(), publicKeyBuffer->size());

    return (bool)ndn::security::verifySignature(data, publicKey);
  }

private:
  std::string m_keyFile;
  char* m_resolvFile;
  ndn::Face m_face;
  const ssize_t FINGERPRINT_OFFSET = -1;
  const ssize_t KEYLOCATOR_PREFIX_OFFSET = -2;
};

} // end namespace core
} // end namespace ndnssec

#endif // NDNSSEC_CORE_ABSTRACT_CONSUMER_HPP
